import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Components/Header';
import Footer from './Components/Footer';

function App() {
  return (
    <>
      <div>
        <Header />
      </div>
      <div>
        <Footer />
      </div>
    </>
  );
}

export default App;
