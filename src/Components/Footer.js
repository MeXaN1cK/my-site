import React, { Component } from 'react';
import { Container, Image } from 'react-bootstrap';
import logo from './logo192.png'

export default class Footer extends Component {
    render() {
        return (
            <>
                <Container fluid style={{ backgroundColor: '#212529', color: '#fff' }}>
                    <Container style={{ display: 'flex', justifyContent: 'center', padding: '10px' }}>
                        <p>
                            Developed by <a href="https://gitlab.com/MeXaN1cK">MeXaN1cK</a>,
                            Powered by <a href="https://ru.reactjs.org/">
                                <Image src={logo} height="30"
                                    width="30"
                                    className="d-inline-block align-top"
                                />
                                React.js
                            </a>
                        </p>
                    </Container>
                </Container>
            </>
        )
    }
}