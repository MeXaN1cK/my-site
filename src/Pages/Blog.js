import React, { Component } from 'react';
import { Col, Container, Card, Button, ListGroup, Row } from 'react-bootstrap';

class Blog extends Component {
    render() {
        return (
            <>
                <Container className='mt-3'>
                    <Row>
                        <Col md="9">
                            <ListGroup variant="flush">
                                <ListGroup.Item>
                                    <Card>
                                        <Card.Img variant="top" />
                                        <Card.Body>
                                            <Card.Title>Title</Card.Title>
                                            <Card.Text>
                                                Text
                                            </Card.Text>
                                            <Button variant="info">Button</Button>
                                        </Card.Body>
                                    </Card>
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <Card>
                                        <Card.Img variant="top" />
                                        <Card.Body>
                                            <Card.Title>Title</Card.Title>
                                            <Card.Text>
                                                Text
                                            </Card.Text>
                                            <Button variant="info">Button</Button>
                                        </Card.Body>
                                    </Card>
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <Card>
                                        <Card.Img variant="top" />
                                        <Card.Body>
                                            <Card.Title>Title</Card.Title>
                                            <Card.Text>
                                                Text
                                            </Card.Text>
                                            <Button variant="info">Button</Button>
                                        </Card.Body>
                                    </Card>
                                </ListGroup.Item>
                            </ListGroup>
                        </Col>
                        <Col md="3">
                            <h5 className="text-center mt-5">Categories</h5>
                            <Card>
                                <ListGroup variant="flush">
                                    <ListGroup.Item>Java</ListGroup.Item>
                                    <ListGroup.Item>Kotlin</ListGroup.Item>
                                    <ListGroup.Item>Scala</ListGroup.Item>
                                    <ListGroup.Item>Lua</ListGroup.Item>
                                    <ListGroup.Item>C</ListGroup.Item>
                                    <ListGroup.Item>C++</ListGroup.Item>
                                    <ListGroup.Item>Javascript</ListGroup.Item>
                                    <ListGroup.Item>Red</ListGroup.Item>
                                    <ListGroup.Item>Ruby</ListGroup.Item>
                                </ListGroup>
                            </Card>
                            <Card className="mt-3" bg="light">
                                <Card.Body>
                                    <Card.Title>Side widget</Card.Title>
                                    <Card.Text>Text</Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}

export default Blog;