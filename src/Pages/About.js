import React, { Component } from 'react';
import { Container, Nav, Tab, Row, Col, ListGroup } from 'react-bootstrap';

class About extends Component {
    render() {
        return (
            <Container>
                <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                    <Row>
                        <Col sm={3}>
                            <Nav variant="pills" className="flex-column mt-2">
                                <Nav.Item>
                                    <Nav.Link eventKey="first" >Technologies</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey="second" >Programming</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey="third" >Contacts</Nav.Link>
                                </Nav.Item>
                            </Nav>
                        </Col>
                        <Col sm={9}>
                            <Tab.Content className="mt-3">
                                <Tab.Pane eventKey="first">
                                    <p>
                                        Technologies
                                    </p>
                                </Tab.Pane>
                                <Tab.Pane eventKey="second">
                                    <Col>
                                        <ListGroup variant="flush">
                                            <ListGroup.Item>Java</ListGroup.Item>
                                            <ListGroup.Item>Kotlin</ListGroup.Item>
                                            <ListGroup.Item>Scala</ListGroup.Item>
                                            <ListGroup.Item>Lua</ListGroup.Item>
                                            <ListGroup.Item>C</ListGroup.Item>
                                            <ListGroup.Item>C++</ListGroup.Item>
                                            <ListGroup.Item>Javascript</ListGroup.Item>
                                            <ListGroup.Item>Red</ListGroup.Item>
                                            <ListGroup.Item>Ruby</ListGroup.Item>
                                        </ListGroup>
                                    </Col>
                                </Tab.Pane>
                                <Tab.Pane eventKey="third">
                                    <p>
                                        Contacts
                                    </p>
                                </Tab.Pane>
                            </Tab.Content>
                        </Col>
                    </Row>
                </Tab.Container>
            </Container>
        );
    }
}

export default About;