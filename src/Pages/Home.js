import React, { Component } from 'react';
import MatrixRain from '../Components/MatrixRain';
import '../../src/App.css';
import { Container } from 'react-bootstrap';
import Canvas from '../Components/Canvas';

class Home extends Component {
    render() {
        const draw = (ctx, frameCount) => {
            const w = ctx.canvas.width;
            const h = ctx.canvas.height
            const katakana = 'アァカサタナハマヤャラワガザダバパイィキシチニヒミリヰギジヂビピウゥクスツヌフムユュルグズブヅプエェケセテネヘメレヱゲゼデベペオォコソトノホモヨョロヲゴゾドボポヴッン';
            const latin = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            const nums = '0123456789';
            const alphabet = katakana + latin + nums;
            const cols = Math.floor(w / 20) + 1;
            const ypos = Array(cols).fill(0);

            function matrix() {
                ctx.fillStyle = '#0001';
                ctx.fillRect(0, 0, w, h);

                ctx.fillStyle = '#0f0';
                ctx.font = '10pt matrixFont';

                ypos.forEach((y, ind) => {
                    const text = alphabet.charAt(Math.floor(Math.random() * alphabet.length));
                    const x = ind * 20;
                    ctx.fillText(text, x, y);
                    if (y > 100 + Math.random() * 10000) ypos[ind] = 0;
                    else ypos[ind] = y + 20;
                });
                
            }
            setInterval(matrix, 1000);

            // ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
            // ctx.fillStyle = '#000000'
            // ctx.beginPath()
            // ctx.arc(50, 100, 20 * Math.sin(frameCount * 0.05) ** 2, 0, 2 * Math.PI)
            // ctx.fill()
        }
        setInterval(draw, 1000)

        return <Canvas draw={draw} style={{ height: window.innerHeight/2, width: window.innerWidth/2 }} />
    }
}

export default Home;